package lanceur;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import entites.Etudiant;
import entites.Promotion;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Configuration depuis le fichier hibernate.cfg.xml
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure()
				.build();
		
		SessionFactory sessionFactory = null;
		try {
			sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
		}
		catch (Exception e) {
			StandardServiceRegistryBuilder.destroy( registry );
		}
		
		// Ouverture de la session Hibernate
		Session session = sessionFactory.openSession();

		////////////////////////////
		// Ins�rer dans la table  //
		////////////////////////////
		session.beginTransaction();
		
		Promotion promotion = new Promotion();
		promotion.setNom("IBEA 5");
		
		Etudiant etudiant = new Etudiant();
		etudiant.setNom("CLAVET");
		etudiant.setPrenom("Thomas");
		GregorianCalendar gc = new GregorianCalendar(1978, 0, 28);
		etudiant.setDateNaissance(gc.getTime());
		promotion.getEtudiants().add(etudiant);
		session.save(etudiant);
		
//		etudiant = new Etudiant();
//		etudiant.setNom("GARFLOR");
//		etudiant.setPrenom("St�phane");
//		gc = new GregorianCalendar(1978, 0, 28);
//		etudiant.setDateNaissance(gc.getTime());
//		promotion.getEtudiants().add(etudiant);		
//		session.save(etudiant);
//		
//		etudiant = new Etudiant();
//		etudiant.setNom("FLOR");
//		etudiant.setPrenom("Estelle");
//		gc = new GregorianCalendar(1978, 0, 28);
//		etudiant.setDateNaissance(gc.getTime());
//		promotion.getEtudiants().add(etudiant);		
//		session.save(etudiant);
		
		session.flush();
		session.getTransaction().commit();

		//////////////////////////////////
		// Mettre � jour dans la table  //
		//////////////////////////////////
		session.beginTransaction();
	
		// Un attribut
		etudiant = (Etudiant) session.get(Etudiant.class, 2L);
		etudiant.setPrenom("Richard");
		session.flush();
		session.getTransaction().commit();
	
		etudiant = (Etudiant) session.load(Etudiant.class, 3L);	
		
		////////////////////////////////////////////////
		// Supprimer un enregistrement dans la table  //
		////////////////////////////////////////////////
		session.beginTransaction();
		
		etudiant = (Etudiant) session.load(Etudiant.class, 6L);
		session.delete(etudiant);
		
		session.flush();
		session.getTransaction().commit();		
		
		// Requ�ter sur la base
		Query query = session.createQuery("from Etudiant");
		List<Etudiant> result = query.list();

		for (Iterator<Etudiant> iterator = result.iterator(); iterator.hasNext();) {
			Etudiant etudiantObj = iterator.next();
			System.out.println(etudiantObj.toString());
		}
		
		// Requ�ter sur la base avec des param�tres		
		query = session.createQuery("from Etudiant e where e.nom = :libelle");
		query.setString("libelle", "IMA5");
		result = query.list();

		for (Iterator<Etudiant> iterator = result.iterator(); iterator.hasNext();) {
			Etudiant etudiantObj = (Etudiant) iterator.next();
			System.out.println(etudiantObj.toString());
		}
		
		// Requ�ter sur la base avec des param�tres		
		Criteria criteria = session.createCriteria(Etudiant.class);
		criteria.add(Restrictions.like("nom", "FLOR", MatchMode.ANYWHERE));
		result = criteria.list();
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			Etudiant etudiantObj = (Etudiant) iterator.next();
			System.out.println(etudiantObj.toString());
		}
		
		session.close();
		sessionFactory.close();
		
	}

}
