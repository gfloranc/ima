package entites;

import java.util.HashSet;
import java.util.Set;

public class Promotion {
	
	private Long id;
	
	private String nom;
	
	private Set<Etudiant> etudiants = new HashSet<Etudiant> ();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Set<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(Set<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

}
