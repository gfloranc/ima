--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-09-15 21:49:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2000 (class 1262 OID 17254)
-- Name: test; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';


ALTER DATABASE test OWNER TO postgres;

\connect test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 174 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2003 (class 0 OID 0)
-- Dependencies: 174
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 17261)
-- Name: etudiant; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etudiant (
    id integer,
    nom character varying,
    prenom character varying,
    datenaissance date,
    promotion_id integer
);


ALTER TABLE etudiant OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 17255)
-- Name: promotion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE promotion (
    id integer,
    nom character varying,
    annee character varying
);


ALTER TABLE promotion OWNER TO postgres;

--
-- TOC entry 1995 (class 0 OID 17261)
-- Dependencies: 173
-- Data for Name: etudiant; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 1994 (class 0 OID 17255)
-- Dependencies: 172
-- Data for Name: promotion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2002 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-09-15 21:49:17

--
-- PostgreSQL database dump complete
--

