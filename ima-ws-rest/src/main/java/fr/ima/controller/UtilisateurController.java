package fr.ima.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.ima.model.Utilisateur;
import fr.ima.repository.UtilisateurRepository;

@RestController // Annotation qui indique que chaque m�thide renvoie un objet dans la r�ponse : RestController = Controller + ResponseBody
@RequestMapping(value="/authentificationService")
public class UtilisateurController {

    @Resource
    private UtilisateurRepository utilisateurRepository;
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
    public List<Utilisateur> getAll(){
        return utilisateurRepository.recupererTousLesUtilisateurs();
    }

    @RequestMapping(value="/user/{login}", method=RequestMethod.GET)
    public Utilisateur getUser(@PathVariable String login){
        
    	return utilisateurRepository.recupererUtilisateur(login);
    }    
}
