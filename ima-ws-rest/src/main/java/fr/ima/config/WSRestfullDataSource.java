package fr.ima.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

@Configuration
@ComponentScan("fr.ima")
public class WSRestfullDataSource {

    @Bean
    public DataSource dataSource(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.addScript("bdd/schema.sql");
        builder.addScript("bdd/data.sql");
        return  builder.build();
    }

    @Bean
    public JdbcTemplate template(){
        return new JdbcTemplate(dataSource());
    }
	
}
