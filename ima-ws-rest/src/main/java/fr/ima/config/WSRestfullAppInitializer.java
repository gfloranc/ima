package fr.ima.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WSRestfullAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();		

        // Déclaration des classes de configuration 
        rootContext.register(WSRestfullConfig.class, WSRestfullDataSource.class);
        servletContext.addListener(new ContextLoaderListener(rootContext));
		   
        // Initialisation de la webapp MVC
        AnnotationConfigWebApplicationContext dispatcherServlet = 
		                               new AnnotationConfigWebApplicationContext();
        dispatcherServlet.register(WSRestfullConfig.class);

        // Déclaration du dispatcher de servlet
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", 
		                                      new DispatcherServlet(dispatcherServlet));
        dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}
}
