package fr.ima.repository;

import java.util.List;

import fr.ima.model.Utilisateur;

public interface UtilisateurRepository {
	
	public List<Utilisateur> recupererTousLesUtilisateurs ();
	
	public Utilisateur recupererUtilisateur (String login) ;

}
