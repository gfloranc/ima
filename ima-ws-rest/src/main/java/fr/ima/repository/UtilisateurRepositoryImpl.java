package fr.ima.repository;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import fr.ima.model.Utilisateur;

@Repository // D�finit la classe comme transactionnel et permet d'injecter JdbcTemplate
public class UtilisateurRepositoryImpl implements UtilisateurRepository {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public List<Utilisateur> recupererTousLesUtilisateurs() {
			return template.query("select * from utilisateur", BeanPropertyRowMapper.newInstance(Utilisateur.class));
	}

	@Override
	public Utilisateur recupererUtilisateur(String login) {
		return (Utilisateur) template.queryForObject("select * from utilisateur where login = ?", new Object [] {login}, new BeanPropertyRowMapper<>(Utilisateur.class));
	}

}
