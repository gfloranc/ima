package fr.ima;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.ima.config.WSRestfullConfig;
import fr.ima.config.WSRestfullDataSource;
import fr.ima.config.WSRestfullDataSourceTest;
import fr.ima.model.Utilisateur;
import fr.ima.repository.UtilisateurRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration // A ajouter uniquement lorsque l'on code le WS. 
@ContextConfiguration(classes={WSRestfullConfig.class, WSRestfullDataSourceTest.class})
public class UtilisateurRepositoryTest {

	@Resource
	private UtilisateurRepository repository;
	
	@Test
	public void recupererTousLesUtilisateurs() {
		final List<Utilisateur> utilisateurs = repository.recupererTousLesUtilisateurs();
		
		// Assertions
        Assert.assertNotNull(utilisateurs);
        Assert.assertTrue(!utilisateurs.isEmpty());
	}
	
	@Test
	public void recupererUtilisateur() {
		final String login = "gfloranc";
		final Utilisateur utilisateur = repository.recupererUtilisateur(login);
		
		// Assertions
		Assert.assertNotNull(utilisateur);
		Assert.assertEquals(login, utilisateur.getLogin());
	}
}
