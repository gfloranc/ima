package exemple.impl;

import java.rmi.RemoteException;

import exemple.pub.Modulo;

public class ServeurModulo implements Modulo {

	@Override
	public int compute(int valeur, int modulo) throws RemoteException {
		
		return valeur % modulo;
	}

	@Override
	public String quiSuisJe() throws RemoteException {
		return "Je suis Greg";
	}
}
