package exemple.lanceur;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import exemple.impl.ServeurModulo;
import exemple.pub.Modulo;

public class Lanceur {
	
    public static void main(String args[]) {
    	
    	try {
    		
    		// Cr�ation de l'objet qui sera distribu�
    		ServeurModulo obj = new ServeurModulo();
    		// Retourne le stub qui sera pass� au client
    	    Modulo stub = (Modulo) UnicastRemoteObject.exportObject(obj, 0);
    	    // Enregistre le stud dans le registre
    	    Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
    	    registry.bind("ModuloCalc", stub);

    	    System.out.println("Serveur pr�t");
    	} catch (Exception e) {
    	    System.err.println("Exception: " + e.toString());
    	    e.printStackTrace();
    	}
    }
}
