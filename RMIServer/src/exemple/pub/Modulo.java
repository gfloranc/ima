package exemple.pub;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Modulo extends Remote {

	public int compute(int valeur, int modulo) throws RemoteException;
	
	public String quiSuisJe() throws RemoteException;
	
}
