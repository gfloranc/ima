package exemple.lanceur;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Lancement de tous les tests");
		suite.addTestSuite(TestRMI.class);
		return suite;
	}

}
