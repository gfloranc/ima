package exemple.lanceur;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import exemple.pub.Modulo;

public class LanceurClient {

    public static void main(String[] args) {

    	try {
    	    Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
    	    Modulo stub = (Modulo) registry.lookup("ModuloCalc");
    	    int reponse = stub.compute(123, 7);
    	    System.out.println("reponse: " + reponse);
//    	    System.out.println(stub.quiSuisJe());
    	    
    	} catch (Exception e) {
    	    System.err.println("Client exception: " + e.toString());
    	    e.printStackTrace();
    	}
    }	
}
