package exemple.lanceur;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import exemple.pub.Modulo;
import junit.framework.TestCase;

public class TestRMI extends TestCase {

	public void testCalculModulo() {
		
		try {
			Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			Modulo stub = (Modulo) registry.lookup("ModuloCalc");
			assertEquals(4, stub.compute(123, 7));
		} catch(Exception e){
			e.printStackTrace();
		}
		

	}
	
	
	
}
