package ima;

import javax.ejb.Stateful;

/**
 * Session Bean implementation class TestStatefullBean
 */
@Stateful
public class TestStatefullBean implements TestStatefullBeanRemote {

	private String user = "";
    /**
     * Default constructor. 
     */
    public TestStatefullBean() {
    }

	@Override
	public String getUser() {
		return user;
		
	}

	@Override
	public void setUser(String _user) {
		user = _user;
	}

}
