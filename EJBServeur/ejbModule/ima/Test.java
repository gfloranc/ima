package ima;

import javax.ejb.Remote;

@Remote
public interface Test {
	
	int getModulo (int valeur, int diviseur);

}
