package ima;

import javax.ejb.Stateless;
import ima.Test;

@Stateless
public class TestBean implements Test {

	@Override
	public int getModulo(int valeur, int diviseur) {
		return valeur % diviseur;
	}

}
