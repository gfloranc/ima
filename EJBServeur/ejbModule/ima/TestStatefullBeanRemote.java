package ima;
import javax.ejb.Remote;

@Remote
public interface TestStatefullBeanRemote {

	public String getUser();

	public void setUser(String _user);	
	
}
