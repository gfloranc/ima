
package ima;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import ima.beans.Adresse;
import ima.beans.Etudiant;

@WebService(endpointInterface = "ima.EtudiantService")
public class EtudiantServiceImpl implements EtudiantService {

	public List<Etudiant> rechercherEtudiant(String codePromotion, String annee) {
		
		// Ceci est un bouchon.
		final List<Etudiant> etudiant = new ArrayList<Etudiant> ();
		
		final Adresse adr = new Adresse();
		adr.setCodePostal("49000");
		adr.setRue("44 rue Rabelais");
		adr.setVille("ANGERS");
		
		Etudiant etud = new Etudiant();
		etud.setNom("DRONET");
		etud.setPrenom("Maxime");
		etud.setAdresse(adr);
		etudiant.add(etud);
		
		etud = new Etudiant();
		etud.setNom("RAMBAUD");
		etud.setPrenom("Antoine");
		etud.setAdresse(adr);
		etudiant.add(etud);
		
		etud = new Etudiant();
		etud.setNom("Zhang");
		etud.setPrenom("Chong");
		etud.setAdresse(adr);
		etudiant.add(etud);
		
		etud = new Etudiant();
		etud.setNom("Hmamouch");
		etud.setPrenom("Mohammed-Abderrahmane");
		etud.setAdresse(adr);
		etudiant.add(etud);
		
		return etudiant;
	}

}

