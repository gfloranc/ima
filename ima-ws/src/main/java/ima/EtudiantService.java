package ima;

import java.util.List;

import javax.jws.WebService;

import ima.beans.Etudiant;

@WebService
public interface EtudiantService {
	List<Etudiant> rechercherEtudiant (String codePromotion, String annee);
}

