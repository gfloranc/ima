package fr.ima.sikuli;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class DemoSikuli {

	public static void main(String[] args) {
		
		// D�finit l'endroit o� est install� le driver qui g�re Chrome
		System.setProperty("webdriver.chrome.driver", "C:\\Outils\\chromedriver\\chromedriver.exe");		
		
		// Instancie un objet ChromeDriver qui permet de commander Chrome
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ecosia.org/");

		// Permet de faire une pause pour simuler le comportement de l'utilisateur
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Objet pour interagir avec l'�cran (vision Sikuli)
		Screen screen = new Screen();
		Pattern zoneTexte = new Pattern("\\images\\zoneTexte.PNG");
		try {
			screen.type(zoneTexte, "IMA Angers");
		} catch (FindFailed e2) {
		// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		// Permet de faire une pause pour simuler le comportement de l'utilisateur
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Identifie la loupe et clique dessus
		Pattern loupe = new Pattern("\\images\\loupe.PNG");
		try {
			screen.click(loupe);
		} catch (FindFailed e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		

		// Clic sur le lien IMA
		WebElement lien = driver.findElement(By.linkText("IMA | Institut de Math�matiques Appliqu�es"));	
		lien.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		driver.close();
		driver.quit();
	}
}
