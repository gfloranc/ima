package ima.beans;

import java.util.Date;

import javax.validation.constraints.Size;


public class User {
	
	private Date dateConnexion;
	
	@Size(min=3, max=10) 
	private String login;
	
	@Size(min=5, max=10) 
	private String password;

	private String nom;
	
	private String prenom;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getDateConnexion() {
		return dateConnexion;
	}
	public void setDateConnexion(Date dateConnexion) {
		this.dateConnexion = dateConnexion;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
}
