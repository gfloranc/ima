
package ima.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ima.beans.User;

@Controller // D�clare la classe comme un controller Spring MVC
public class HomeController {

	
    @RequestMapping(value = "/", method = RequestMethod.GET) // Traite les requ�tes de type GET d'URL "/"
    public String showIndex(Model model) {
    	model.addAttribute(new User());
   	 
        return "index";
    }

}
