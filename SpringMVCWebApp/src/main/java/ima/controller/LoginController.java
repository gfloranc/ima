package ima.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.support.RequestContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import ima.beans.User;
import ima.service.AuthentificationService;
import ima.service.EtudiantService;

@Controller // D�clare la classe comme un controller Spring MVC
@RequestMapping("/authentification") // Traite les requ�tes "/authentification"
public class LoginController{
	
	@Autowired // Indique que l'injection de d�pendance doit �tre ex�cut�e
	private EtudiantService dao;
	
	@Autowired
	private AuthentificationService authentification;
	
	@RequestMapping(method = RequestMethod.POST) // M�thode invoqu�e si requ�te de type POST
	public String printWelcome(@Valid User user, BindingResult result, Model model) {
		
		if (!result.hasErrors()){
			
			final boolean auth = authentification.authentifier(user.getLogin());
			
			if (auth) {
				
				user.setDateConnexion(new Date());
				
				RequestContextHolder.currentRequestAttributes().setAttribute("user", user, RequestAttributes.SCOPE_SESSION);
				model.addAttribute("message", "Bienvenue : " + user.getLogin());
				model.addAttribute("etudiants",  dao.lister());
				return "menu";
			} else {
				return "index";
			}
			
		} else {
			// Retour � la page index
			return "index";//"redirect:/";
		}
	}
}
