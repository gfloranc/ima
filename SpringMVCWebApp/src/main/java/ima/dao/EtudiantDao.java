package ima.dao;

import java.util.List;

import ima.dao.entities.Etudiant;

public interface EtudiantDao {

	public Etudiant chercherParNom(String nom);
	
	public List<Etudiant> lister();

}
