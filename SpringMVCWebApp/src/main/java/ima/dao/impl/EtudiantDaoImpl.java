package ima.dao.impl;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import ima.dao.EtudiantDao;
import ima.dao.entities.Etudiant;

@Repository // St�r�otype Spring qui indique que la classe sera en charge de l'�change avec la BDD
public class EtudiantDaoImpl extends HibernateDaoSupport implements EtudiantDao {
	
	public EtudiantDaoImpl(){}
	
	@Autowired // Indique que l'injection de d�pendance doit �tre ex�cut�e
	public void init(SessionFactory factory) {
	    setSessionFactory(factory);
	}
	
	@Override
	public Etudiant chercherParNom(String nom) {
		
		Etudiant etudiant = null;
		
		final List<Etudiant> etudiants = (List<Etudiant>) getSessionFactory().getCurrentSession().createCriteria(Etudiant.class)
				.add(Restrictions.eq("nom", nom)).list();

		if (!etudiants.isEmpty()){
			etudiant = etudiants.get(0);
		};
		
		return etudiant;
	}

	@Override
	public List<Etudiant> lister() {
		return (List<Etudiant>)  getSessionFactory().getCurrentSession().createCriteria(Etudiant.class).list();
	}

}
