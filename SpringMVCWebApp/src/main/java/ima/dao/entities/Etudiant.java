package ima.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "etudiant") // D�clare la classe comme une entit� persistante et pr�cise la table associ�e
public class Etudiant implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8675827050594952405L;

	@Id // propri�t� servant d'id
    @Column(nullable = false) // propri�t� id ne doit pas �tre nulle
	private Integer id;
	
    @Column // propri�t� qui mappe avec la colonne nom. Pr�ciser le nom de la colonne si diff�rente du nom de l'attribut
	private String nom;

    @Column
	private String prenom;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
