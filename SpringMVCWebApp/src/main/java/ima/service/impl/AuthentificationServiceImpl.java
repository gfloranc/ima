package ima.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ima.beans.User;
import ima.service.AuthentificationService;

@Service // D�clare la classe comme une classe de service 
public class AuthentificationServiceImpl implements AuthentificationService {
	
//	private static final List<String> USERS = Arrays.asList(new String[]{"chong", "mohamed", "antoine", "maxime", "gregory"}); 
	
	@Override
	public boolean authentifier(String user) {
		
		boolean ret = false;
		
		// Utilisation de RestTemplate
		final RestTemplate restTemplate = new RestTemplate();
		final User authentUser = restTemplate.getForObject("http://localhost:8080/ima-ws-rest-0.0.1-SNAPSHOT/authentificationService/user/{login}", 
															User.class, 
															user);
		
		if (authentUser != null) {
			ret = true;
		}
		
		return ret;
	}

}
