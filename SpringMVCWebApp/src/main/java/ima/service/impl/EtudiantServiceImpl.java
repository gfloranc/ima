package ima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ima.dao.EtudiantDao;
import ima.dao.entities.Etudiant;
import ima.service.EtudiantService;

@Service // D�clare la classe comme une classe de service 
@Transactional // D�l�gue � spring-transaction la gestion des transactions et applique la gestion des 
				// transactions sur toutes les m�thodes publiques de la classe
public class EtudiantServiceImpl implements EtudiantService {

    @Autowired
    private EtudiantDao etudiantDao;	
	
	@Override
    @Transactional(readOnly = true) // sp�cifie une gestion sp�cifique de la transaction pour cette m�thode. La transaction ici est en lecture seule
	public Etudiant chercherParNom(String nom) {

		return etudiantDao.chercherParNom(nom);
	}

	
	@Override
    @Transactional(readOnly = true)
	public List<Etudiant> lister() {

		final List<Etudiant> etudiants = etudiantDao.lister();
		return etudiants;
	}
}
