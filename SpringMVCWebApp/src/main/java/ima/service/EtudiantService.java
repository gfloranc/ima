package ima.service;

import java.util.List;

import ima.dao.entities.Etudiant;

public interface EtudiantService {
	
	public Etudiant chercherParNom(String nom);
	
	public List<Etudiant> lister();
}
