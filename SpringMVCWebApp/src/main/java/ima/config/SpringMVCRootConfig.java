package ima.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration // D�finit la classe comme un composant de configuration
@ComponentScan("ima") // Configure le chemin du package � partir duquel Spring devra scanner pour trouver les annotations Spring
public class SpringMVCRootConfig {

}
