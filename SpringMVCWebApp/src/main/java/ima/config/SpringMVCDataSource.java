package ima.config;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration // D�finit la classe comme un composant de configuration
@EnableTransactionManagement // Active la prise en charge des transactions JDBC par Spring
public class SpringMVCDataSource  {
	
	/**
	 * Bean de gestion des acc�s � la BDD.
	 * @return
	 */
	@Bean // Ex�cute la m�thode et enregistre le r�sultat dans la BeanFactory
	public BasicDataSource dataSource() {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUrl("jdbc:postgresql://localhost:5432/test");
		ds.setUsername("postgres");
		ds.setPassword("postgres");
		ds.setInitialSize(5);
		ds.setMaxActive(10);
		
		return ds;
	}
	
	/**
	 * Active le mapping OR : Hibernate et PostgreSQL dans notre cas
	 * @param ds
	 * @return
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory(BasicDataSource ds) {
		LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
		sfb.setDataSource(ds);
		sfb.setPackagesToScan(new String [] {"ima.dao.entities"});
		
		Properties props = new Properties();
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		sfb.setHibernateProperties(props);
		return sfb;
	}
    
	/**
	 * Active la gestion des transactions. Spring encapsule les ouvertures et fermetures de transaction
	 * @param sessionFactory
	 * @return
	 */
    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory)
    {
        HibernateTransactionManager txManager = new HibernateTransactionManager(sessionFactory);
        return txManager;
    }    
}
