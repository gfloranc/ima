package ima.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.MvcNamespaceHandler;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import ima.aspect.AspectLog;

@Configuration // D�finit la classe comme un composant de configuration
@EnableWebMvc //Active Spring MVC
@EnableAspectJAutoProxy
@ComponentScan("ima") //D�finit le package racine pour le scan par Spring
public class SpringMVCWebConfig extends WebMvcConfigurerAdapter {
	
	// Configuration du composant qui va g�rer la vue
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		//resolver.setViewClass(InternalResourceView.class); // Tags HTML classiques
		resolver.setViewClass(JstlView.class); // Avec JSTL

		return resolver;
	}
	
	@Bean
	Validator beanValidation() {
	    return new LocalValidatorFactoryBean();
	}	
	
	@Bean
	AspectLog aspectLog() {
		return new AspectLog();
	}

	// Active le filtrage pour intercepter que les URL relative au framework MVC
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	// D�finit les ressources statiques de l'appli web (js, images, etc..)
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/js/**").addResourceLocations("/js/");
	}
	
	

}
