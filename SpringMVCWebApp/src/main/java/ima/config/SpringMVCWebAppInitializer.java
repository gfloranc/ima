package ima.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Classe qui joue le r�le du web.xml
 * @author Greg
 *
 */
public class SpringMVCWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] {SpringMVCRootConfig.class};
	} 

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] {SpringMVCWebConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String [] {"/"};
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		// D�claration du Listener
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("ima.config");
        context.register(SpringMVCWebAppInitializer.class); //D�clare la configuration de type web.xml
	    servletContext.addListener(new ContextLoaderListener(context));
	    
	    // D�claration du DispatcherServlet
	    ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
	    dispatcher.setLoadOnStartup(1);
	    dispatcher.addMapping("/");
	}
}
