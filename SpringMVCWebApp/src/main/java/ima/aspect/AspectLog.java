package ima.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AspectLog {

	  //@Before("within(ima.controller..*)") // Application sur toutes les m�thodes et toutes les classes du package ima.controller
//	  @Before("execution(public * ima.controller.*.*(..))") // Application sur toutes les m�thodes et toutes les classes du package ima.controller	  
//	  public void log(JoinPoint pjp) {
//
//		  Logger.getAnonymousLogger().info("Execute une trace : "+pjp.getSignature().getName());
//	  }	

	  @Pointcut("execution(public * ima.controller.*.*(..))")
	  public void log(JoinPoint pjp) {
		  Logger.getAnonymousLogger().info("Execute une trace : "+pjp.getSignature().getName());		  
	  }
}
