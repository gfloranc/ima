<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<html>
<head>
<title>Formulaire d'inscription</title>
</head>
<body>
	<h1>Appli Web Spring MVC</h1>
	<sf:form method="POST" commandName="user" servletRelativeAction="/authentification">
		<table>
		<tr><td align="right">Login : </td><td><sf:input path="login"  /><sf:errors path="login" cssStyle="color: red;"/></td></tr>
		<tr><td align="right">Mot de passe : </td><td><sf:password path="pwd" /><sf:errors path="pwd" cssStyle="color: red;"/></td></tr>
		</table>
		<input type="submit" value="authenticate" title="S'authentifier" />		
	</sf:form>	
</body>
</html>