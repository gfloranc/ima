<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
      <style>
         #test {
            position: inherit;
         }
         ul#sortables {
            width: 300px;
            margin: 0;
            padding: 0;
         }
         li.sortme {
            padding: 4px 8px;
            color: #fff;
            cursor: pointer;
            list-style: none;
            width: 300px;
            background-color: grey;
            border: 1px solid;
         }
         ul#sortables li {
            margin: 10px 0;
         }
      </style>

    <script type="text/javascript" src="./js/mootools-core.js"></script>
    <script type="text/javascript" src="./js/mootools-more.js"></script>   
   
    <script type="text/javascript">
    window.addEvent('domready', function(){

    	  alert('JS prend la main !');
    	  
          new Sortables($('test'), {
               initialize: function(){
                 var step = 5;
                 
                 this.elements.each(function(element, i) {
                    var color = [step, 82, 87].hsbToRgb();
                    element.setStyle('background-color', color);
                    step = step + 35;
                    element.setStyle('height', 200);
                 });
              }
           });

    	});
    </script>
<title>Administration des étudiants</title>
</head>
<body>
	<h1>${message}</h1>
	<ul id = "test">
	<c:forEach var="item" items="${etudiants}" varStatus="status">
		<li class="sortme">${status.index}&nbsp;${item.prenom}&nbsp;${item.nom}&nbsp;<a href="detail?id=${item.id}" >détail</a></li>
	</tr>
	
	</c:forEach>
	</ul
</body>
</html>