package fr.ima.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class DemoSelenium {

	public static void main(String[] args) {
		
		// D�finit l'endroit o� est install� le driver qui g�re Chrome
		System.setProperty("webdriver.chrome.driver", "C:\\Outils\\chromedriver\\chromedriver.exe");		
		
		// Instancie un objet ChromeDriver qui permet de commander Chrome
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.fr/");

		// Permet de faire une pause pour simuler le comportement de l'utilisateur
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// R�cup�re dans la page web la balise dont l'id s'appelle searchInput
		WebElement zonetexteRecherche = driver.findElement(By.id("lst-ib"));
		// Ecrit dans la zone textuelle "searchInput"
		zonetexteRecherche.sendKeys("IMA Angers");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		
//		// R�cup�re dans la page web l'objet input type="button" 
//		WebElement boutonRecherche = driver.findElement(By.name("btnK"));
//		// Clic sur le bouton
//		boutonRecherche.click();
		
		// Clic sur le lien IMA
		WebElement lien = driver.findElement(By.linkText("IMA | Institut de Math�matiques Appliqu�es"));	
		lien.click();
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		
		// Clic sur le menu Formation en simulant le clic de la souris
		WebElement lienFormation = driver.findElement(By.linkText("FORMATION"));
		Actions actionSouris = new Actions(driver);
		actionSouris.moveToElement(lienFormation).perform();
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		
		// Clic sur le sous-menu Dipl�me IMA
		WebElement lienDiplomeIMA = driver.findElement(By.id("menu-item-115"));	
		lienDiplomeIMA.click();		
		
		driver.close();
		driver.quit();
	}
}
