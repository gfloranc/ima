import org.apache.log4j.Logger;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		Logger log = Logger.getLogger(Main.class);
		
		log.trace("Un message de niveau TRACE");
		log.debug("Un message de niveau DEBUG");
		log.warn("Un message de niveau WARN");
		log.error("Un message de niveau ERROR");
		log.fatal("Un message de niveau FATAL");
		log.info("Un message de niveau INFO");
	}
}
