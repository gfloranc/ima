package test;

import java.util.Properties;

import ima.Test;
import ima.TestStatefullBeanRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Properties props = new Properties();

		props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
		props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
		props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
		
		Context initialContext = null;
	    
	    try
	    {
	      initialContext = new InitialContext(props);
	    } catch (Exception e)
	    {
	      e.printStackTrace();
	      System.exit(2);
	    }
	    
	    try
	    {
	      // EJB Stateless
	      Object objref = initialContext.lookup("java:global/EJBServeur/TestBean");
	      Test test = (Test)PortableRemoteObject.narrow( objref, Test.class);
	      
	      int modulo = test.getModulo(127, 4);
	      System.out.println("La valeur du modulo est : "+modulo);
	      
	      // EJB Statefull
	      // Permier appel
	      objref = initialContext.lookup("java:global/EJBServeur/TestStatefullBean");
	      TestStatefullBeanRemote testStatefull = (TestStatefullBeanRemote)PortableRemoteObject.narrow( objref, TestStatefullBeanRemote.class);
	      testStatefull.setUser("Greg");
	      
	      System.out.println("Le user est : "+testStatefull.getUser());
	      
	    } catch (Exception e)
	    {
	      System.err.println(" Erreur : " + e);
	      System.exit(2);
	    }		
	}

}
